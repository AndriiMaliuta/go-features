package corre

import "fmt"

type contactInfo struct {
	email string
}

type person struct {
	firstName string
	lastName  string
	info      contactInfo
}

func (p person) updateName(newName string) {
	p.firstName = newName
}

func main() {

	alex := person{firstName: "Alex", lastName: "Adams", info: contactInfo{email: "mail"}}
	alex.updateName("Alexis")
	fmt.Println(alex)

	// var vasyl person

}

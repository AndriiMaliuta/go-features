package corre

import (
	"fmt"
	"sort"
)

func Maps1() {
	// simple
	var map1 = map[int]string{1: "Petro"}
	//map1 = append(map1, [1]:"")
	map1[2] = "Vasyl"
	fmt.Println(map1)

	// make
	var employee = make(map[string]int)
	employee["Mark"] = 34
	fmt.Println(employee)
	delete(employee, "Petro") // error
}

func TruncateMap() {
	var employee = map[string]int{"Mark": 10, "Sandy": 20,
		"Rocky": 30, "Rajiv": 40, "Kate": 50}

	// Method - I
	for k := range employee {
		delete(employee, k)
	}

	// Method - II
	employee = make(map[string]int)
}

func SortMap() {
	unSortedMap := map[string]int{"India": 20, "Canada": 70, "Germany": 15}

	keys := make([]string, 0, len(unSortedMap))

	for k := range unSortedMap {
		keys = append(keys, k)
	}
	sort.Strings(keys) // SORT

	for _, k := range keys {
		fmt.Println(k, unSortedMap[k])
	}
}

package corre

import "fmt"

func Arr1() {
	var arr1 [3]int
	x := [5]int{10, 20, 30, 40, 50}   // Intialized with values
	var y [5]int = [5]int{10, 20, 30} // Partial assignment
	arr3 := [...]int{10, 20, 30}
	arrSpecific := [5]int{1: 10, 3: 30}
	fmt.Println(x)

	fmt.Println(arr1)
	fmt.Println(arr3)
	fmt.Println(x)
	fmt.Println(y)
	fmt.Println(arrSpecific)

	countries := [...]string{"India", "Canada", "Japan", "Germany", "Italy"}

	fmt.Printf("Countries: %v\n", countries)
}

package corre

import (
	"fmt"
	"reflect"
)

// https://www.golangprograms.com/go-language/slices-in-golang-programming.html
func Slices1() {

	// simple
	var intSlice1 []int
	var strSlice1 []string

	fmt.Println(reflect.ValueOf(intSlice1).Kind())
	fmt.Println(reflect.ValueOf(strSlice1).Kind())

	// make
	var intSlice = make([]int, 10)        // when length and capacity is same
	var strSlice = make([]string, 10, 20) // when length and capacity is different

	fmt.Printf("intSlice \tLen: %v \tCap: %v\n", len(intSlice), cap(intSlice))
	fmt.Println(reflect.ValueOf(intSlice).Kind())
	fmt.Println(reflect.ValueOf(strSlice).Kind())

	//slice
	var intSlice3 = []int{10, 20, 30, 40}
	var strSlice3 = []string{"India", "Canada", "Japan"}

	fmt.Printf("intSlice \tLen: %v \tCap: %v\n", len(intSlice3), cap(intSlice3))
	fmt.Printf("strSlice \tLen: %v \tCap: %v\n", len(strSlice3), cap(strSlice3))

	// new
	var intSlice4 = new([50]int)[0:10]

	fmt.Println(reflect.ValueOf(intSlice4).Kind())
	fmt.Printf("intSlice \tLen: %v \tCap: %v\n", len(intSlice4), cap(intSlice4))
	fmt.Println(intSlice4)

	// Remove
	strSlice = RemoveIndex(strSlice, 3)

}

func RemoveIndex(s []string, index int) []string {
	return append(s[:index], s[index+1:]...)
}

package interfaces

import "fmt"

type Bot interface {
	GetGreeting() string //(string, error)
}

type englishBot struct {
}
type spanishBot struct {
}

func interfaceTry() {
	eb := englishBot{}
	sb := spanishBot{}
	sb.GetGreeting()
	eb.GetGreeting()
	PrintGreeting(eb)
	PrintGreeting(sb)
}

func PrintGreeting(b Bot) {
	fmt.Println(b.GetGreeting())
}

func (e englishBot) GetGreeting() string {
	// VERY custom logic for generating an english greeting
	return "Hi there!"
}

func (spanishBot) GetGreeting() string {
	return "Hola!"
}

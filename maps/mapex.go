package maps

import "fmt"

func TryMapOne() {
	map1 := map[int]string{1: "one", 2: "two"}
	var map2 map[int]string
	map3 := make(map[int]int)

	for i, s := range map1 {
		fmt.Println(i, s)
	}

	fmt.Println(map2)
	fmt.Println(map3)

}

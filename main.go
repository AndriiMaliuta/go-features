package main

import (
	"fmt"
	"go-features/concur"
	"go-features/myfiles"
	"sync"
	"time"
)

func main() {
	// START
	start := time.Now()
	fmt.Println(start)
	// =========

	myfiles.FilesWalking("/home/andrii/")

	// END
	fmt.Println(time.Now().Sub(start).Milliseconds())
}

func init1000() {
	wg := sync.WaitGroup{}
	concur.ConcurPrint(&wg, "PPP")

	for i := 0; i < 50; i++ {
		go func() {
			wg.Add(1)
			println("Inside ANONYMOUS ")
			defer wg.Done()
		}()
	}
}

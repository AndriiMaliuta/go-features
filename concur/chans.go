package concur

import "fmt"

func ChannelsOne() {
	var (
		a int
		b int
	)

	ints := make(chan int)
	ints <- 3
	ints <- a
	ints <- b
	fmt.Println(a, b)

}

package concur

import (
	"fmt"
	"runtime"
	"strings"
	"sync"
)

func PrintSmth(i int, str string) {
	fmt.Printf("Hello %s at %d \n", str, i)
}

func Concur() {

	runtime.GOMAXPROCS(2)
	//runtime.GOMAXPROCS(runtime.NumCPU())		// as CPU

	var wg sync.WaitGroup
	wg.Add(3)

	fmt.Println("Start Goroutines")

	// anonymous functions
	go func() {
		defer wg.Done()
		for i := 0; i < 200; i++ {
			PrintSmth(i, "Petro")
		}
	}()

	go func() {
		defer wg.Done()
		for i := 0; i < 200; i++ {
			PrintSmth(i, "Vasyl")
		}
	}()

	go func() {
		for i := 0; i < 1000; i++ {
			fmt.Println(strings.Compare("aaaaaaaaaaaaaaaa", "bbbbbbbbbbbbbbbbbbbb"))
		}
	}()

	go func() {
		// Schedule the call to Done to tell main we are done.
		defer wg.Done()

		// Display the alphabet three times
		for count := 0; count < 3; count++ {
			for char := 'A'; char < 'A'+26; char++ {
				fmt.Printf("%c ", char)
			}
		}
	}()

	fmt.Println("Waiting To Finish")
	wg.Wait()
	fmt.Println("Terminating the program")

}

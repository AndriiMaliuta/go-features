package concur

import (
	"context"
	"fmt"
	"sync"
)

func Print2Smth(num int, item string) {
	wg.Add(1)
	fmt.Printf("Hello %s %d", item, num)
	println("")
	defer wg.Done()
}

func ConcurPrint(wg *sync.WaitGroup, item string) {
	for a := 0; a < 1000; a++ {
		println(context.Background())
		Print2Smth(a, item)
	}
	wg.Done()
	println("Done")

}

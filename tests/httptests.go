package tests

import (
	"io/ioutil"
	"net/http"
	"testing"
)

func TestHttp(t *testing.T) {
	resp, err := http.Get("http://example.com")
	body := resp.Body
	bts, err := ioutil.ReadAll(body)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	t.Log("Given the need to test downloading different content.")
	t.Log(string(bts))
}

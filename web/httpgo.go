package web

import (
	"net/http"
	"os"
)

type gResponse struct {
}

func HttpGo() *http.Response {

	resp, err := http.Get("http://google.com")
	defer resp.Body.Close()

	if err != nil {
		os.Exit(1)
	}

	//var gr gResponse
	//
	//err = json.NewDecoder(resp.Body).Decode(&gr)
	//if err != nil {
	//	log.Println(err)
	//}

	return resp
}

package web

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
)

const (
	EXMP_URL = "http://example.com/"
)

func HttpMethod() {
	resp, err := http.Get(EXMP_URL)
	if err != nil {
		os.Exit(1)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	fmt.Println(string(body))
}

func HttpClientGet() {
	req, err := http.NewRequest(http.MethodGet, EXMP_URL, nil)
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("TEST", "TEST")
	//req.Body.Read()
	client := http.Client{}

	resp, err := client.Get(EXMP_URL)
	//var respBytes []byte
	//resp.Body.Read(respBytes)
	//fmt.Println(string(respBytes))
	resp2, err := ioutil.ReadAll(resp.Body)

	fmt.Println(string(resp2))
}

func HttpClientDo() {
	req, err := http.NewRequest(http.MethodGet, EXMP_URL, nil)
	if err != nil {
		fmt.Println(err)
	}
	client := http.Client{}
	resp, err := client.Do(req)

	readAll, err := io.ReadAll(resp.Body)

	fmt.Println(string(readAll))
}

package myfiles

import (
	"errors"
	"fmt"
	"io/fs"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

func ReaderOne() {
	fileBytes, err := ioutil.ReadFile("./text.txt")
	if err != nil {
		log.Panicln(err)
	}
	s := string(fileBytes)
	fmt.Println(s)
}

func WriteOne() {
	str := "Hello there, file!"
	bts := []byte(str)
	err := ioutil.WriteFile("t2.txt", bts, fs.ModeAppend)
	if err != nil {
		log.Panicln(err)
	}
}

func FilesWalking(dirName string) {
	println(os.UserHomeDir())
	err := filepath.Walk(dirName, func(path string, info fs.FileInfo, err error) error {
		fmt.Println(path, info.Name())
		return fmt.Errorf("error")
	})
	if err != nil {
		log.Println(err)
	}
}

type MyReader1 struct {
}

func (r MyReader1) Read(p []byte) (n int, err error) {
	fmt.Println(fmt.Sprintf("Chaned: %s", string(p)))
	return 3, errors.New("empty name")
}

//func MyParseDir() {
//	filepath.Walk("/home/andrii/docs", func(path string, info fs.FileInfo, err error) error {
//
//	}
//}
